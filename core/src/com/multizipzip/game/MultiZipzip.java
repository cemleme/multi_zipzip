package com.multizipzip.game;

import com.badlogic.gdx.Game;
import com.multizipzip.game.Views.MainMenuScreen;

public class MultiZipzip extends Game {

	@Override
	public void create () {
    	Assets.init();
    	Assets.load();
		Assets.manager.finishLoading();

		setScreen(new MainMenuScreen(this));
	}


}
