package com.multizipzip.game.Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.net.ServerSocket;
import com.badlogic.gdx.net.ServerSocketHints;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.multizipzip.game.Assets;
import com.multizipzip.game.MultiZipzip;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainMenuScreen extends AbstractScreen {

	private Skin skin;
	private Label header;
	private ImageButton mainButton;
	private TextureRegion test;
    private Socket connection;

	private String serverAddress = "46.101.165.64";
	//private String serverAddress = "localhost";

	// constructor to keep a reference to the main Game class
	public MainMenuScreen(MultiZipzip game) {
		super(game);
		skin = Assets.manager.get(Assets.skin, Skin.class);
    }

    private void connect() {
		if (connection == null || !connection.isConnected()) {
			SocketHints hints = new SocketHints();
			hints.tcpNoDelay = true;
			connection = Gdx.net.newClientSocket(Net.Protocol.TCP, serverAddress, 8080, hints);
		}
    }

	@Override
	public void render(float delta) {
		super.render(delta);
		stage.act(delta);
		

        spriteBatch.begin();
       // spriteBatch.draw(test,Virtual_Width/2,Virtual_Height/2,Virtual_Width/4,Virtual_Width/4);
        spriteBatch.end();
		
		stage.draw();
	}


	@Override
	public void show() {
		super.show();
		test = skin.getAtlas().findRegion("button_play");
		
		mainButton = new ImageButton(skin, "playButton");
		mainButton.setPosition(Virtual_Width/2,Virtual_Height/2);
		mainButton.setSize(Virtual_Width/4,Virtual_Height/4);
		
		header = new Label("Main Menu", skin,"siyah_buyuk");
		header.setPosition(200, 500);
		
		stage.addActor(mainButton);
        stage.addActor(header);

        this.connect();

		mainButton.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if ( connection.isConnected() ) {
                    game.setScreen(new PlayScreen(game, connection));
                }
			}
		});
	}
	

 	@Override
 	public void goToPreviousScreen(){
 		Gdx.app.exit();
 	}

}