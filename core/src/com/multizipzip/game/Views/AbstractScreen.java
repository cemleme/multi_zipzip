package com.multizipzip.game.Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.multizipzip.game.MultiZipzip;
import com.multizipzip.game.Variables;

public class AbstractScreen implements Screen {
	
	public MultiZipzip game;
	public float Virtual_Width= Variables.Virtual_Width;
	public float Virtual_Height= Variables.Virtual_Height;
	public FitViewport viewport;
	public SpriteBatch spriteBatch;
	public Stage stage;
	public OrthographicCamera cam;
	
	public AbstractScreen(MultiZipzip game){
		this.game = game;
		spriteBatch = new SpriteBatch();
		viewport = new FitViewport(Virtual_Width, Virtual_Height);
		stage = new Stage(viewport, spriteBatch);
		cam = new OrthographicCamera();
	}
	
	@Override
	public void render(float delta) {
    	Gdx.gl.glClearColor(255 / 255.0f, 255 / 255.0f, 255 / 255.0f, 1); 
 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void show() {

		viewport = new FitViewport(Virtual_Width, Virtual_Height);
		stage = new Stage(viewport, spriteBatch);
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Virtual_Width, Virtual_Height);
		
        InputProcessor backProcessor = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
            	
                if ((keycode == Keys.BACKSPACE) || (keycode == Keys.BACK)){
                	goToPreviousScreen();
                }
                return false;
            }
        };

        InputMultiplexer multiplexer = new InputMultiplexer(backProcessor,stage);
        Gdx.input.setInputProcessor(multiplexer);
	}
	
	public void goToPreviousScreen(){}

	@Override
	public void dispose() {
        spriteBatch.dispose();
        stage.dispose();
	}
	
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}
}