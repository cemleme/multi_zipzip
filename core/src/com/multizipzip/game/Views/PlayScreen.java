package com.multizipzip.game.Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.multizipzip.game.Assets;
import com.multizipzip.game.MultiZipzip;
import com.multizipzip.game.Variables;
import com.multizipzip.game.Objects.Ball;
import com.multizipzip.game.Objects.Ground;
import com.multizipzip.game.Objects.Player;
import com.multizipzip.game.Objects.Player.PlayerState;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PlayScreen extends AbstractScreen {

	private Skin skin;
	private Label header;
	private ImageButton mainButton;
	private TextureRegion test;
	
    private World world;
    private Box2DDebugRenderer debugRenderer;
    private ShapeRenderer shapeRenderer;
    
    private Player player1, player2;
    private Ball ball;

    private Socket connection;

	private Label countDownLabel;
	private Label player1ScoreLabel;
	private Label player2ScoreLabel;
    
	public PlayScreen(MultiZipzip game,Socket connection) {
		super(game);
		skin = Assets.manager.get(Assets.skin, Skin.class);
        this.connection = connection;
		
		initBox2D();
		shapeRenderer = new ShapeRenderer();
		player1 = new Player(world, 100, 500, "left", shapeRenderer);
		player2 = new Player(world, 900, 500, "right", shapeRenderer);
		ball = new Ball(world, shapeRenderer);
		new Ground(world);

        this.syncToServer();
	}
	
	public void initBox2D(){
		debugRenderer = new Box2DDebugRenderer();
        world = new World(new Vector2(0, -98f), true);
	}

    public void syncToServer(){
        new Thread() {
            @Override
            public void run() {
                try {
                    BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    while(true) {
                        String jsonText = buffer.readLine();
                        JsonValue states = new JsonReader().parse(jsonText);
                        for (JsonValue state = states.child; state != null; state = state.next) {
							//Gdx.app.log("received:", state.toString());
							if (state.getString("c").equals("p")) {
								float x = state.getFloat("x");
								float y = state.getFloat("y");
								if (state.getString("id").equals("player1")){
									player1.serverPosition.y = y;
									player1.serverPosition.x = x;
								}else if(state.getString("id").equals("player2")){
									player2.serverPosition.y = y;
									player2.serverPosition.x = x;
								}else if(state.getString("id").equals("ball")){
									ball.serverPosition.y = y;
									ball.serverPosition.x = x;
								}
							}else if(state.getString("c").equals("cd")){
								int time = state.getInt("time");
								countDownLabel.setVisible(time >= 0);
								countDownLabel.setText(""+time);
							}else if(state.getString("c").equals("end")){
								countDownLabel.setVisible(true);
								countDownLabel.setText("game ended");
							}else if(state.getString("c").equals("s")){
								player1ScoreLabel.setText(""+state.getInt("p1"));
								player2ScoreLabel.setText(""+state.getInt("p2"));
							}
                        }
                    }
                } catch (IOException e) {
					Gdx.app.log("syncToServer", "an error occured", e);
                }
            }
        }.start();
    }
	

	@Override
	public void render(float delta) {
		super.render(delta);
		//ball.draw(delta);
		ball.updateFromServer(0);
		//player1.update(delta);
		player1.updateFromServer();
		//player2.update(delta);
		player2.updateFromServer();

		world.step(delta, 6, 2);
		//debugRenderer.render(world, cam.combined);
		stage.act(delta);


		spriteBatch.begin();
		// spriteBatch.draw(test,Virtual_Width/2,Virtual_Height/2,Virtual_Width/4,Virtual_Width/4);
		spriteBatch.end();

		stage.draw();
	}


	@Override
	public void show() {
		super.show();
		test = skin.getAtlas().findRegion("button_play");
		
		mainButton = new ImageButton(skin, "homeButton");
		mainButton.setPosition(900, 600);
		mainButton.setSize(50, 50);
		
		header = new Label("Play Screen", skin,"siyah_buyuk");
		header.setPosition(200, 600);
		
		stage.addActor(mainButton);
        stage.addActor(header);

		this.countDownLabel = new Label("Waiting..", skin,"siyah_buyuk");
		this.countDownLabel.setPosition(200, 400);
		stage.addActor(this.countDownLabel);

		this.player1ScoreLabel = new Label("0", skin,"siyah_buyuk");
		this.player1ScoreLabel.setPosition(50, 400);
		stage.addActor(this.player1ScoreLabel);

		this.player2ScoreLabel = new Label("0", skin,"siyah_buyuk");
		this.player2ScoreLabel.setPosition(900, 400);
		stage.addActor(this.player2ScoreLabel);

		String readyCmd = String.format("[{\"c\":\"r\"}]");
		try {
			sendAction(readyCmd);
		} catch (IOException e) {
			Gdx.app.error("show","connection error",e);
		}

		mainButton.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				String readyCmd = String.format("[{\"c\":\"q\"}]");
				try {
					sendAction(readyCmd);
				} catch (IOException e) {
					Gdx.app.error("show","connection error",e);
				}
				game.setScreen(new MainMenuScreen(game));
			}
		});
		
		
		stage.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                String player = "player2";
				if(x<Virtual_Width/2){
                    player = "player1";
                }
                try {
                    String command = String.format("[{\"c\":\"j\",\"id\":\"%s\",\"type\":\"j\"}]", player);
                    sendAction(command);
                } catch (IOException e) {
                    e.printStackTrace();
                }
				return true;
			}
		});
		
	    world.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
		    	String a = contact.getFixtureA().getUserData().toString();
		    	String b = contact.getFixtureB().getUserData().toString();
		    	
		    	if(a.equals("ground") && b.equals("playerleft")){
		    		player1.setIdle();
		    	}
		    	else if(a.equals("ground") && b.equals("playerright")){
		    		player2.setIdle();
		    	}
			}
			
			@Override
			public void endContact(Contact contact) {
			}
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				/*
		    	String a = contact.getFixtureA().getUserData().toString();
				String b = contact.getFixtureB().getUserData().toString();
				if(b.equals("ball") && (a.equals("playerleft") || a.equals("playerright"))){
					Body bd = contact.getFixtureA().getBody();
					bd.setLinearVelocity(0, bd.getLinearVelocity().y);
				}
				*/
			}
			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}
	    });
	}
	

 	@Override
 	public void goToPreviousScreen(){
 		Gdx.app.exit();
 	}

	private void sendAction(String action) throws IOException {
		String endAction = action + "\r\n";
		connection.getOutputStream().write(endAction.getBytes());
	}

}