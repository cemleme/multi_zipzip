package com.multizipzip.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.multizipzip.game.Variables;

public class Player {
	private World world;
	private Body body;
	private Fixture bodyF, headF;
	private Vector2 startPosition;

    public Vector2 serverPosition;
	
	private float width, height, headRadius;
	private String name;
	private PlayerState state;
	
	public enum PlayerState {
		IDLE,
		JUMPING
	};
	
	private ShapeRenderer shapeRenderer;
	
	public Player(World world, float startX, float startY, String playername, ShapeRenderer shapeRenderer) {
		this.world = world;
		this.shapeRenderer = shapeRenderer;
		name = playername;
		System.out.println("creating player "+name);
		width=30 / Variables.PIXEL_TO_METER;
		height=100 / Variables.PIXEL_TO_METER;
		startPosition = new Vector2(startX / Variables.PIXEL_TO_METER, startY / Variables.PIXEL_TO_METER);
        serverPosition = startPosition.cpy();
		headRadius = 2*width;
		
		state = PlayerState.IDLE;
		
		initBody();
		//body.setLinearVelocity(0,world.getGravity().y);
	}
	
	public void initBody(){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        
        bodyDef.position.set(startPosition);

        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width/2, height/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        fixtureDef.restitution = 0f;

        bodyF = body.createFixture(fixtureDef);
        bodyF.setUserData("player"+name);
        
        shape.dispose();

        CircleShape headShape = new CircleShape();
        headShape.setPosition(new Vector2(0, height/2 + headRadius*3/4f));
        headShape.setRadius(headRadius);
        
        fixtureDef.shape = headShape;
        fixtureDef.density = 1f;
        fixtureDef.restitution = 0.5f;
        
        

		//headPosition = body.getPosition().cpy().add(0, headRadius + height);
        //bodyDef.position.set(headPosition);
        //head = world.createBody(bodyDef);
        headF = body.createFixture(fixtureDef);
        headF.setUserData("player"+name);

        
        
        body.setFixedRotation(true);
       // head.setFixedRotation(true);
        
        
        headShape.dispose();
        

        /*
         * 
         * 
         * 
        DistanceJointDef distanceJD = new DistanceJointDef();
        distanceJD.bodyA = head;
        distanceJD.bodyB = body;
        distanceJD.localAnchorA.set(new Vector2(0, 0));
        distanceJD.localAnchorB.set(new Vector2(0, height));
        distanceJD.length = headRadius;
        distanceJD.collideConnected = true;
        world.createJoint(distanceJD);
         */
	}
	
	public void setIdle(){
		state = PlayerState.IDLE;
	}
	
	public void fixPosX(){
		if(body.getPosition().x != startPosition.x) body.setTransform(startPosition.x, body.getPosition().y, 0);
	}
	
	public void update(float delta){
		fixPosX();
		//System.out.println("pos: "+body.getPosition().y);
		Vector2 headPosReal = body.getPosition().cpy().add(0, height/2 + headRadius*3/4f).scl(Variables.PIXEL_TO_METER);
		Vector2 bodyPosReal = body.getPosition().cpy().sub(width/2, height/2).scl(Variables.PIXEL_TO_METER);
		
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.RED);
		shapeRenderer.circle(headPosReal.x, headPosReal.y, headRadius * Variables.PIXEL_TO_METER);
		shapeRenderer.rect(bodyPosReal.x, bodyPosReal.y, width * Variables.PIXEL_TO_METER, height * Variables.PIXEL_TO_METER);
		shapeRenderer.end();
	}

    public void updateFromServer() {
        //System.out.println("pos: "+body.getPosition().y);
        Vector2 headPosReal = this.serverPosition.cpy().add(0, height/2 + headRadius*3/4f).scl(Variables.PIXEL_TO_METER);
        Vector2 bodyPosReal = this.serverPosition.cpy().sub(width/2, height/2).scl(Variables.PIXEL_TO_METER);

        shapeRenderer.begin(ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.circle(headPosReal.x, headPosReal.y, headRadius * Variables.PIXEL_TO_METER);
        shapeRenderer.rect(bodyPosReal.x, bodyPosReal.y, width * Variables.PIXEL_TO_METER, height * Variables.PIXEL_TO_METER);
        shapeRenderer.end();
    }
	
	public void act(){
		
		if(state == PlayerState.JUMPING){
			/*
			head.setTransform(head.getPosition().x + .75f, head.getPosition().y - 1, -15);
			
			Timer.schedule(new Task(){
		 		   @Override
		 		   public void run(){
		 			   head.setLinearVelocity(0, 0);
		 			   headPosition = body.getPosition().cpy().add(0, headRadius + height);
		 			   head.setTransform(headPosition, 0);
		 		   }
			}, 0.3f);
			*/
		}
		else if(state == PlayerState.IDLE){
			state = PlayerState.JUMPING;
			body.applyForceToCenter(0, 15000, true);
		}
	}


}
