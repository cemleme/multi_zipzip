package com.multizipzip.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.multizipzip.game.Variables;

public class Ground {
	private World world;
	private Body groundBody;
	
	public Ground(World world) {
		this.world = world;
		initBody();
	}
	
	public void initBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		
		//BOUNDRIES OF MAP
		Vector2[] groundVertices=null;
		
		groundVertices = new Vector2[5];
		
		//GET 4 CORNERS OF THE WORLD
		groundVertices[0]=new Vector2(0,0);
		groundVertices[1]=new Vector2(0, Variables.Virtual_Height / Variables.PIXEL_TO_METER);
		groundVertices[2]=new Vector2(Variables.Virtual_Width / Variables.PIXEL_TO_METER, Variables.Virtual_Height / Variables.PIXEL_TO_METER);
		groundVertices[3]=new Vector2(Variables.Virtual_Width / Variables.PIXEL_TO_METER, 0);
		groundVertices[4]=new Vector2(0,0);
		
		//ground shape
		bodyDef.type=BodyType.StaticBody;
		bodyDef.position.set(0,1);
		
		ChainShape groundShape = new ChainShape();
		
		groundShape.createChain(groundVertices);
		
		fixtureDef.shape=groundShape;
		fixtureDef.friction=0.1f;
		fixtureDef.restitution=0;
		//fixtureDef.filter.maskBits &= ~0x0002;
		
		groundBody = world.createBody(bodyDef);
		groundBody.createFixture(fixtureDef).setUserData("ground");
		groundShape.dispose();
	}
}
