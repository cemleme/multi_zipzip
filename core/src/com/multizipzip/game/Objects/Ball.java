package com.multizipzip.game.Objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.multizipzip.game.Variables;

public class Ball {

	private Body body;
	private World world;
	
	private float radius;
	private Vector2 startPosition;
	
	private ShapeRenderer shapeRenderer;

    public Vector2 serverPosition;
	
	public Ball(World world, ShapeRenderer shapeRenderer) {
		this.world = world;
		this.shapeRenderer = shapeRenderer;
		
		radius = 20 / Variables.PIXEL_TO_METER;
		startPosition = new Vector2(Variables.Virtual_Width/2 / Variables.PIXEL_TO_METER, Variables.Virtual_Height/2 / Variables.PIXEL_TO_METER);
		initBody();
        serverPosition = startPosition.cpy();
		body.applyForceToCenter(1000, 1000, true);
	}
	
	public void initBody(){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        
        bodyDef.position.set(startPosition);

        body = world.createBody(bodyDef);

        CircleShape shape = new CircleShape();
        shape.setRadius(radius);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0f;
        fixtureDef.restitution = 1f;
        fixtureDef.friction = 0;
        
        body.createFixture(fixtureDef).setUserData("ball");
        
        shape.dispose();
        
	}
	
	public void draw(float delta){
		Vector2 ballPosReal = body.getPosition().cpy().scl(Variables.PIXEL_TO_METER);

		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.circle(ballPosReal.x, ballPosReal.y, radius * Variables.PIXEL_TO_METER);
		shapeRenderer.end();
	}

    public void updateFromServer(float delta){
        Vector2 ballPosReal = serverPosition.cpy().scl(Variables.PIXEL_TO_METER);

        shapeRenderer.begin(ShapeType.Filled);
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.circle(ballPosReal.x, ballPosReal.y, radius * Variables.PIXEL_TO_METER);
        shapeRenderer.end();
    }
}
