package com.multizipzip.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Assets {
	
	public static AssetManager manager;
	public static String zipzipPack;
	public static String skin;
	public static Preferences prefs;
	
	public static void init(){
		
		prefs = Gdx.app.getPreferences("prefs");

		manager = new AssetManager();
		//zipzipPack ="data/skin.atlas";
		skin ="skin/skin.json";		
	}
	
	
	public static void load(){
		//manager.load(zipzipPack, TextureAtlas.class);
		manager.load(skin, Skin.class);
	}
	
	public static void dispose(){
		manager.dispose();
	}
}